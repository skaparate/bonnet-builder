# Bonnet Builder

Build wool hats with specific designs.

The idea was to build an app to let the users choose their type of hat and colors that then were sent to the user and shop owner to make the hat.

This was built for a specific brand at the time, but since Flash is now "obsolete", I'm sharing the code now so others may see it.

The project was built with Adobe Flash CS6 and working as of 2019.

[The working project can be seen here](https://nicomv.com/flash/bonnet-builder/), although You need to have flash player installed and working, which may be a problem.

## License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
